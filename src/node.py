#!/usr/bin/env python
import yaml

from reconfig.srv import *
import rospy
import os

def handle_restart_ros(req):
    print "Restarting nodes"
    nodes = rospy.get_param('/reconfig/restart_nodes')
    #nodes = os.popen("rosnode list").readlines()
    #for i in range(len(nodes)):
     # nodes[i] = nodes[i].replace("\n","")

    for node in nodes:
      #print "Node found: " + node
      os.system("rosnode kill "+ node)
    return RestartRosResponse(True)

def handle_motor_reconfigure(req):
    data = dict(
        arduino = dict(
            motor_config = req.config
        )
    )
    filename = rospy.get_param('/reconfig/config_filename')
    with open(filename, 'w') as outfile:
      yaml.dump(data, outfile, default_flow_style=False)
    print "Reconfiguring motors []"
    os.system("rosparam load "+filename )
    return MotorReconfigureResponse(True)

def motor_reconfigure_server():
    rospy.init_node('motor_reconfigure_server')
    s = rospy.Service('reconfigure_motors', MotorReconfigure, handle_motor_reconfigure)
    s = rospy.Service('restart_ros', RestartRos, handle_restart_ros)
    print "Ready to reconfigure motors."
    rospy.spin()

if __name__ == "__main__":
    motor_reconfigure_server()